<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\File;

class GeneratorTest extends TestCase
{
    use WithFaker;

    public function testCreate()
    {
        $data = [
            'products' => [
                [
                  'name' => $this->faker->name,
                  'price' => 1444,
                  'picture' => 'http://best.seller.ru/product_page.asp?pid=12345',
                  'category' => 'Мелкая техника для кухни'
                ],
                [
                    'name' => $this->faker->name,
                    'price' => 1555,
                    'picture' => 'http://best.seller.ru/product_page.asp?pid=12345',
                    'category' => 'Бытовая техника'
                ]
            ]
        ];

        $response = $this->post('api/files', $data);

        $response->assertJsonStructure(['status', 'message']);

        if ($response['status'] == 'success') {
            $id = File::all()->last()->id;
            $link = route('files.show', ['file' => $id]);
            $response->assertJson([
                'message' => 'Ваш запрос принят можете проверить статус по ссылке: '.$link
            ]);
        }
    }

    public function testShow()
    {
        $file = File::all()->random();

        $url = route('files.show', ['file' => $file->id]);

        $response = $this->get($url);

        $response->assertJsonStructure(['status', 'message']);

        if ($response['status'] == 'success') {
            $link = $file->url;
            $response->assertJson([
                'message' => 'Файл успешно сгенерирован, ссылка: '.$link
            ]);
        }
    }
}
