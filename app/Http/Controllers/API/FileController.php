<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\File;
use App\Jobs\GenerateYML;

class FileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'products' => 'required|array',
            'products.*.name' => 'required|max:50',
            'products.*.price' => 'required|integer|min:1',
            'products.*.picture' => 'required|url|max:100',
            'products.*.category' => 'required|max:30'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $products = $request->input('products');

        $file = new File();
        $file->status = 'new';
        $file->save();

        GenerateYML::dispatch($products, $file)->afterResponse();

        $link = route('files.show', ['file' => $file->id]);
        $message = 'Ваш запрос принят можете проверить статус по ссылке: ' . $link;

        return response()->json(['status' => 'success',
                                 'message' => $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'products' => 'required|array',
            'products.*.name' => 'required|max:50',
            'products.*.price' => 'required|integer|min:1',
            'products.*.picture' => 'required|url|max:100',
            'products.*.category' => 'required|max:30'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $products = $request->input('products');

        $file = File::find($id);
        $file->status = 'in-progress';
        $file->save();

        GenerateYML::dispatch($products, $file)->afterResponse();

        $link = route('files.show', ['file' => $file->id]);
        $message = 'Запрос на обновление принят можете проверить статус по ссылке: ' . $link;

        return response()->json(['status' => 'success',
                                 'message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::find($id);

        switch ($file->status) {
            case 'success':
                $message = 'Файл успешно сгенерирован, ссылка: ' . $file->url;
                break;
            case 'new':
                $message = 'Статус файла: новый';
                break;
            case 'in-progress':
                $message = 'Генерация в процессе';
                break;
            case 'failed':
                $message = 'Не удалось сгенерировать файл';
                break;
            default:
                $message = 'Статус генерации неизвестен';
        }
        return response()->json(['status' => $file->status,
                                 'message' => $message]);
    }
}
