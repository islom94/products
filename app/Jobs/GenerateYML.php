<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\File;
use Mockery\CountValidator\Exception;

class GenerateYML implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $products = [];
    private $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $products, File $file)
    {
        $this->products = $products;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->file->status = 'in-progress';
        $this->file->save();

        try {
            $yml = View::make('yml', ['products' => $this->products])->render();
            $filename = $this->file->name ?? Str::random(10).'.yml';
            Storage::disk('public')->put($filename, $yml);
            $this->file->url = Storage::disk('public')->url($filename);
            $this->file->name = $filename;
            $this->file->status = 'success';
        } catch(Exception $e) {
            $this->file->status = 'failed';
        }
        $this->file->save();
    }
}
