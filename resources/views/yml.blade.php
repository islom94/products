<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="2019-11-01 17:22">
    <shop>
        <name>BestSeller</name>
        <company>Tne Best inc.</company>
        <url>http://best.seller.ru</url>
        <currencies>
            <currency id="RUR" rate="1"/>
        </currencies>
        <categories>
            <category id="1">Бытовая техника</category>
            <category id="2">Мелкая техника для кухни</category>
        </categories>
        <offers>
            @foreach ($products as $product)
                <offer id="{{ $loop->iteration }}">
                    <name>{{ $product['name'] }}</name>
                    <url>http://best.seller.ru/product_page.asp?pid=12345</url>
                    <price>{{ $product['price'] }}</price>
                    <currencyId>RUR</currencyId>
                    <categoryId>1</categoryId>
                    <delivery>true</delivery>
                    <category>{{ $product['category'] }}</category>
                </offer>
            @endforeach
        </offers>
    </shop>
</yml_catalog>